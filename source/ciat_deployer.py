# Copyright (C) 2015  Codethink Limited
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.

BUILT_SYSTEMS_FILE = "built_systems.yaml"

def buildcomplete(properties):
    import yaml, os
    global BUILT_SYSTEMS_FILE

    system = {
            'name': properties['system'],
            'slave': properties['slave'],
            'definitions_sha': properties['definitions_sha']}

    if os.path.isfile(BUILT_SYSTEMS_FILE):
        with open(BUILT_SYSTEMS_FILE, 'r') as f:
            built_systems = yaml.load(f)
        if not built_systems:
            built_systems = []
    else:
        built_systems = []
    if system in built_systems:
        return
    built_systems.append(system)
    with open(BUILT_SYSTEMS_FILE, 'w') as f:
        f.write(yaml.dump(built_systems,default_flow_style=False))
