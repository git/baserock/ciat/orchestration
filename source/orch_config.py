# Copyright (C) 2015  Codethink Limited
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.

import ciatlib

categories = [
        'repo_update']

buildslave_scripts = 'git://git.baserock.org/baserock/ciat/buildslave-scripts.git'
buildslave_source = ciatlib.master.GitSource(buildslave_scripts,'master')

columns = [
        ciatlib.master.Column(
                name = "Integration",
                source_repo = buildslave_source,
                category = "repo_update",
                trigger = 'firehose_trigger.sh',
                slavenames=["local-slave"],
                properties = [('repo_name','no repo name given'),
                              ('ref','no ref given')])]

web_user = "codething"
web_user_password = "password"
definitions_base_ref = "refs/heads/cu010-trove/br6/genivi-demo-platform"
candidate_refs = {"refs/heads/cu010-trove/br6/firehose-test-1":"local-slave",
                  "refs/heads/cu010-trove/arm-base":"arm-slave"}
slaves = [
        {'name':'local-slave','password':'pass'},
        {'name':'arm-slave','password':'pass'}]
