# Copyright (C) 2015  Codethink Limited
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.

def fetch_config():
    ''' fetch config from remote repo '''
    import subprocess, os
    # if dir exists remove it
    if os.path.isdir('ciatconfig'):
        subprocess.call(['rm', '-rf', 'ciatconfig'])
    CONFIG_URL = "ssh://git@git.baserock.org/baserock/ciat/ciatconfig"
    cmd = ['git','clone',CONFIG_URL]
    return subprocess.call(cmd)

def validate_config(config,*keys):
    ''' raise an exception if the dictionary is not as expected '''
    for key in keys:
        config[key]

def load_slave_type_configs():
    ''' load the slave type configs to know which slaves to connect to'''
    import yaml, os
    REPO_DIR = 'ciatconfig/slave-types'
    slave_types = []
    for slavetype in os.listdir(REPO_DIR):
        if not slavetype.endswith('.yaml'): continue
        slavetype_path = os.path.join(REPO_DIR,slavetype)
        with open(slavetype_path, 'r') as f:
            config = yaml.load(f)
        validate_config(config, 'name','arch')
        slave_types.append(config)
    return slave_types

def load_pipeline_configs():
    ''' load the pipelines '''
    import yaml, os
    REPO_DIR = 'ciatconfig/pipelines'
    pipelines = []
    for pipeline in os.listdir(REPO_DIR):
        if not pipeline.endswith('.yaml'): continue
        pipeline_path = os.path.join(REPO_DIR,pipeline)
        with open(pipeline_path, 'r') as f:
            config = yaml.load(f)
        validate_config(config,
                'name',
                'candidate-refs',
                'slave-type',
                'clusters',
                'steps')
        pipelines.append(config)
    return pipelines

def get_categories():
    ''' given a list of pipelines, return a list of their categories '''
    global pipelines
    categories = []
    for pipeline in pipelines:
        categories += pipeline.categories
    return categories

def get_columns():
    ''' given a list of pipelines, return a list of their categories '''
    global pipelines
    columns = []
    for pipeline in pipelines:
        columns += pipeline.columns
    return columns

def get_candidate_refs():
    global pipelines
    candidate_refs = []
    for pipeline in pipelines:
        candidate_refs += pipeline.candidate_refs
    return candidate_refs

def pipeline_from_candidate_ref(ref):
    global pipelines
    for p in pipelines:
        if ref in p.candidate_refs:
            return p

def configure():
    from ciatlib.master import pipeline_from_dict
    global slave_types
    global pipelines
    fetch_exit_val = fetch_config()
    if fetch_exit_val: exit(fetch_exit_val)
    slave_types = load_slave_type_configs()
    pipeline_configs = load_pipeline_configs()
    pipelines = []
    for pipeline in pipeline_configs:
        pipelines.append(pipeline_from_dict(pipeline))

if __name__ == '__main__':
    configure()
