#!/usr/bin/env python2.7

# Copyright (C) 2015  Codethink Limited
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.

# This script provides an interface for sandboxed code to trigger buildbot
# using plain HTTP

from bottle import post, request, run, HTTPResponse
import imp
import ciat_deployer
orch_config = imp.load_source('orch_config', '../source/orch_config.py')
configure = imp.load_source('configure', '../source/configure.py')

LOGFILE = '../orch.log'
DEFINITIONS = 'ssh://git@git.baserock.org/baserock/baserock/definitions'
TEST_REPO = 'ssh://git@git.baserock.org/baserock/ciat/ciat-tester'

log_file = open(LOGFILE,'a')

def log(msg):
    ''' write message to log file with timestamp and script name '''
    import datetime
    global log_file
    msg = str(msg)
    dt = str(datetime.datetime.now()).split('.')[0]
    to_log = "[%s] Bottlerock: %s" % (dt, msg)
    print to_log
    log_file.write('%s\n' % to_log)

def sendchange(category,properties):
    ''' sendchange to buildbot with category and a dictionary of property names
        and their values '''

    import subprocess
    IP="127.0.0.1"
    bb_port=9999
    user='orchestration'
    password='orchestration'
    cmd = [
            '../orchenv-master/bin/buildbot',
            'sendchange',
            '-m%s:%d' % (IP,bb_port),
            '-a%s:%s' % (user,password),
            '-Wscriptbot', 
            '-C%s' % category]
    for property in properties.items():
        cmd.append('-p%s:%s' % property)
    log(cmd)
    if subprocess.call(cmd):
        return HTTPResponse(status=500)
    else: return 0

class Status400(Exception): pass

def get_form(*properties):
    ''' get properties from POST form and return as a dict, if property not
        sent in form raise Status400 '''

    property_dict = {}
    for property in properties:
        p = request.forms.get(property)
        if p: 
            property_dict[property] = p
        else:
            raise Status400(property)
    return property_dict

def missing_property_response(property):
    return HTTPResponse(
            status=400,
            body="400: A %s is required" % property)

@post('/repo_update')
def repo_update():
    print request.json
    repo_name = "no repo name"
    for url in request.json['urls']:
        if url.startswith('ssh'):
            repo_name = url
            break
    changes = request.json['changes']
    for change in changes:
        ref = change['ref']
        ref = ref.replace('refs/heads/','')
        sha = change['new']
        if repo_name == DEFINITIONS:
            configure.configure()
            candidate_refs = configure.get_candidate_refs()
            if ref == orch_config.definitions_base_ref:
                # if baseref changes then trigger firehose
                properties = {'repo_name':repo_name,'ref':ref}
                return sendchange('repo_update',properties)
            elif ref in candidate_refs:
                # if a candidate ref changes then trigger that pipeline
                pipeline = configure.pipeline_from_candidate_ref(ref)
                # TODO get the system needed from the cluster given
                if 'base-system-armv8l64.morph' in pipeline.clusters:
                    system = 'base-system-armv8l64.morph'
                else:
                    system = 'genivi-demo-platform-x86_64-generic.morph'
                properties = {
                        "ref":ref,
                        "sha":sha,
                        "system": system,
                        "pipeline":pipeline.name}
                return sendchange(
                        category = '%s Build' % pipeline.name,
                        properties = properties)
        elif repo_name == TEST_REPO:
            # if the tests change then force a build
            force = {"ref":"force","sha":sha}
            return sendchange('genivi-demo-platform-x86_64 Build',force)
        else:
            properties = {'repo_name':repo_name,'ref':ref}
            return sendchange('repo_update',properties)

@post('/force_build')
def force_build():
    force = {"ref":"force"}
    return sendchange('genivi-demo-platform-x86_64 Build',force)

@post('/build_complete')
def build_complete():
    try:
        properties = get_form(
                "system",
                "slave",
                "buildslave_scripts_sha",
                "definitions_sha",
                "testing_sha",
                "pipeline")
    except Status400 as p:
        return missing_property_response(p)
    ciat_deployer.buildcomplete(properties)
    return sendchange('%s Image(s)' % properties['pipeline'],properties)

@post('/deploy_complete')
def deploy_complete():
    try:
        properties = get_form(
                'artefact',
                'testing_sha',
                'buildslave_scripts_sha',
                'definitions_sha',
                "pipeline")
    except Status400 as p:
        return missing_property_response(p)
    return sendchange('%s Test' % properties['pipeline'],properties)

@post('/testing_complete')
def testing_complete():
    try:
        properties = get_form(
                'artefact',
                'testing_sha',
                'buildslave_scripts_sha',
                'definitions_sha',
                "pipeline")
    except Status400 as p:
        return missing_property_response(p)
    return sendchange('%s Publish' % properties['pipeline'],properties)

if __name__ == '__main__':
    run(host='0.0.0.0', port=8080, debug=True)
