#!/bin/sh
set -e
echo "Orchestration setup starting"
# create master
echo "Buildbot Master: Creating virtual environment"
virtualenv --no-site-packages orchenv-master
cd orchenv-master
echo "Buildbot Master: Installing dependencies"
./bin/pip install buildbot pyyaml
./bin/buildbot create-master -r master
cp ../source/master.cfg master/master.cfg
git clone ssh://git@git.baserock.org/baserock/ciat/ciatlib
cd ciatlib
../bin/python setup.py install
# create slave
cd ../..
echo "Buildbot Local Slave: Creating virtual environment"
virtualenv --no-site-packages orchenv-slave
cd orchenv-slave
echo "Buildbot Local Slave: Installing dependencies"
./bin/pip install buildbot-slave
./bin/buildslave create-slave --umask=022 -r slave localhost:9989 local-slave pass
# setup bottlerock
cd ..
echo "Bottlerock: Creating virtual environment"
virtualenv --no-site-packages bottlerock
cd bottlerock
echo "Bottlerock: Installing dependencies"
./bin/pip install buildbot bottle pyyaml
git clone ssh://git@git.baserock.org/baserock/ciat/ciatlib
cd ciatlib
../bin/python setup.py install
echo "Setup Complete"
